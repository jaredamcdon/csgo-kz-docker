###########################################################
# Dockerfile that builds a CSGO Gameserver
###########################################################
FROM cm2network/steamcmd:root

LABEL maintainer="jaredamcdon@gmail.com"

ENV STEAMAPPID 740
ENV STEAMAPP csgo
ENV STEAMAPPDIR "${HOMEDIR}/${STEAMAPP}-dedicated"
ENV DLURL https://raw.githubusercontent.com/CM2Walki/CSGO
ENV METAMOD_VERSION 1.10
ENV SOURCEMOD_VERSION 1.10

# Create autoupdate config
# Add entry script & ESL config
# Remove packages and tidy up

RUN echo "deb http://security.debian.org/ buster/updates main contrib non-free" > /etc/apt/sources.list \
&& echo "deb-src http://security.debian.org/ buster/updates main contrib non-free" >> /etc/apt/sources.list \
&& echo "deb-src http://ftp.us.debian.org/debian/ buster main contrib non-free" >> /etc/apt/sources.list \
&& echo "deb http://ftp.us.debian.org/debian/ buster main contrib non-free" >> /etc/apt/sources.list

RUN apt-get update \
&& apt-get install -y unzip \
wget \
ca-certificates \
lib32z1

COPY entry-pre.sh /home/steam/temp_dir/entry-pre.sh
RUN chmod a+x /home/steam/temp_dir/entry-pre.sh

RUN mkdir -p "${STEAMAPPDIR}" \
	&& wget --max-redirect=30 "${DLURL}/master/etc/entry.sh" -O "${HOMEDIR}/entry.sh" \
	&& { \
		echo '@ShutdownOnFailedCommand 1'; \
		echo '@NoPromptForPassword 1'; \
		echo 'login anonymous'; \
		echo 'force_install_dir '"${STEAMAPPDIR}"''; \
		echo 'app_update '"${STEAMAPPID}"''; \
		echo 'quit'; \
	   } > "${HOMEDIR}/${STEAMAPP}_update.txt" \
	&& chmod +x "${HOMEDIR}/entry.sh" \
	&& chown -R "${USER}:${USER}" "${HOMEDIR}/entry.sh" "${STEAMAPPDIR}" "${HOMEDIR}/${STEAMAPP}_update.txt" \	
	&& rm -rf /var/lib/apt/lists/* 
	
ENV SRCDS_FPSMAX=300 \
	SRCDS_TICKRATE=128 \
	SRCDS_PORT=27015 \
	SRCDS_TV_PORT=27020 \
	SRCDS_CLIENT_PORT=27005 \
	SRCDS_NET_PUBLIC_ADDRESS="0" \
	SRCDS_IP="0" \
	SRCDS_MAXPLAYERS=14 \
	SRCDS_TOKEN=0 \
	SRCDS_RCONPW="changeme" \
	SRCDS_PW="changeme" \
	SRCDS_STARTMAP= \
	SRCDS_REGION=3 \
	SRCDS_MAPGROUP= \
	SRCDS_GAMETYPE=0 \
	SRCDS_GAMEMODE=1 \
	SRCDS_HOSTNAME="New \"${STEAMAPP}\" Server" \
	SRCDS_WORKSHOP_START_MAP=0 \
	SRCDS_HOST_WORKSHOP_COLLECTION="598964669" \
	SRCDS_WORKSHOP_AUTHKEY="" \
	ADDITIONAL_ARGS=""



USER ${USER}

#VOLUME ${STEAMAPPDIR}

WORKDIR ${HOMEDIR}

RUN bash /home/steam/temp_dir/entry-pre.sh

####      #      #  #  ####
#   custom script by us   #
####      #      #  #  ####

SHELL ["/bin/bash", "-c"]

RUN mkdir -p /home/steam/csgo-dedicated/csgo/ /home/steam/temp_dir
USER root
RUN chown -R steam /home/steam
USER ${USER}

# download and unzip kztimer
RUN version=$(sed 's/.*">//g' <<< $(curl https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/?tab=downloads | grep "Full.zip" | head -1)) \
&& version=$(sed 's|</a>||g' <<< $version) \
&& link="https://bitbucket.org/kztimerglobalteam/kztimerglobal/downloads/$version" \
&& wget $link -O /home/steam/temp_dir/kzt.zip \
&& unzip /home/steam/temp_dir/kzt.zip -d /home/steam/temp_dir/kzt \
&& rm /home/steam/temp_dir/kzt.zip

# download and unzip dhooks
RUN wget "https://forums.alliedmods.net/attachment.php?attachmentid=190123&d=1625050030" -O /home/steam/temp_dir/dhooks.zip \
&& unzip /home/steam/temp_dir/dhooks.zip -d /home/steam/temp_dir/dhooks \
&& rm /home/steam/temp_dir/dhooks.zip

#download FixHintColoerMessages plugins
RUN wget https://github.com/Franc1sco/FixHintColorMessages/blob/master/FixHintColorMessages.smx?raw=true -O /home/steam/temp_dir/FixHintColorMessages.smx \
&& wget https://raw.githubusercontent.com/Franc1sco/FixHintColorMessages/master/FixHintColorMessages.sp -O /home/steam/temp_dir/FixHintColorMessages.sp

#save map list script to container
#COPY ./getMaps.sh /home/steam/temp_dir/getMaps.sh

# move modified databases.cfg to temp_dir
COPY ./databases.cfg /home/steam/temp_dir/databases.cfg

USER root
RUN mkdir -p "${HOMEDIR}/${STEAMAPP}-dedicated/csgo/addons/sourcemod/plugins/" \
&& mkdir -p "${HOMEDIR}/${STEAMAPP}-dedicated/csgo/addons/sourcemod/scripting/" \
&& chmod -R 777 "${HOMEDIR}/${STEAMAPP}-dedicated/"

RUN cp -f /home/steam/temp_dir/FixHintColorMessages.smx "${HOMEDIR}/${STEAMAPP}-dedicated/csgo/addons/sourcemod/plugins/" \
&& cp -f /home/steam/temp_dir/FixHintColorMessages.sp "${HOMEDIR}/${STEAMAPP}-dedicated/csgo/addons/sourcemod/scripting/"

# move unzipped files to proper directory
RUN cp -rf /home/steam/temp_dir/kzt/* /home/steam/csgo-dedicated/csgo/
RUN cp -rf /home/steam/temp_dir/dhooks/* /home/steam/csgo-dedicated/csgo/
RUN cp -f /home/steam/temp_dir/databases.cfg /home/steam/csgo-dedicated/csgo/addons/sourcemod/configs/databases.cfg

#get map names and save them to two files
#RUN chmod a+x /home/steam/temp_dir/getMaps.sh \
#&& bash /home/steam/temp_dir/getMaps.sh

COPY mapcycle.txt /home/steam/temp_dir/mapcycle.txt
RUN cp -f /home/steam/temp_dir/mapcycle.txt /home/steam/temp_dir/maplist.txt
RUN cp -f /home/steam/temp_dir/mapcycle.txt /home/steam/csgo-dedicated/csgo/mapcycle.txt
RUN cp -f /home/steam/temp_dir/maplist.txt /home/steam/csgo-dedicated/csgo/maplist.txt

# move smx files to proper directory
RUN mv /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/disabled/rockthevote.smx /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/rockthevote.smx
RUN mv /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/disabled/mapchooser.smx /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/mapchooser.smx
RUN mv /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/disabled/nominations.smx /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/nominations.smx
RUN mv /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/nextmap.smx /home/steam/csgo-dedicated/csgo/addons/sourcemod/plugins/disabled/nextmap.smx

# increase timelimit in main.cfg
#RUN sed -i 's/mp_timelimit 5/mp_timelimit 60/g' /home/steam/csgo-dedicated/csgo/addons/sourcemode/configs/main.cfg 
COPY ./main.cfg /home/steam/temp_dir/main.cfg
RUN mkdir -p /home/steam/csgo-dedicated/csgo/addons/sourcemode/configs/ \
&& cp -f /home/steam/temp_dir/main.cfg /home/steam/csgo-dedicated/csgo/addons/sourcemode/configs/main.cfg \
&& chmod 555 /home/steam/csgo-dedicated/csgo/addons/sourcemode/configs/main.cfg

# add remaining file subscriptions to subscribed_collection_ids.txt
RUN touch /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt \
&& echo "598964669" >  /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt \
&& echo "599552865" >>  /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt \
&& echo "599556167" >>  /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt \
&& echo "897415592" >>  /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt

# copy subscribed_collection_ids.txt to subscribed_file_ids.txt
RUN cp -f /home/steam/csgo-dedicated/csgo/subscribed_collection_ids.txt /home/steam/csgo-dedicated/csgo/subscribed_file_ids.txt

####   #      #      ####
#   end custom script   #
####   #      #      ####

COPY ./entry-2.sh /home/steam/temp_dir/entry-2.sh

RUN chmod a+x /home/steam/temp_dir/entry-2.sh \
&& chown steam /home/steam/temp_dir/entry-2.sh

CMD bash /home/steam/temp_dir/entry-2.sh

#CMD ["bash", "sleep infinity"]

# Expose ports
EXPOSE 27015/tcp \
	27015/udp \
	27020/udp

